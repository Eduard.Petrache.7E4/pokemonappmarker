package com.example.mapsprojectreal.daos


import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.MediaStore
import com.example.bottomnav.daos.Marker
import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import org.mongodb.kbson.ObjectId
import java.io.ByteArrayOutputStream
import java.util.*


class MarkersDao(val realm: Realm, val userID: String) {
    fun listFlow() : Flow<List<Marker>> = realm.query<Marker>().find().asFlow().map {
        it.list.toList()
    }

    fun insertItem(text: String, latitude: String, longitude: String){
        realm.writeBlocking {
            val marker = Marker(name = text, latitude = latitude, longitude = longitude, owner_id = userID )
            copyToRealm(marker)
        }
    }

    fun updateTitle(markerId: String, newTitle: String) {
        realm.writeBlocking {
            val marker = this.query<Marker>("_id == $0", ObjectId(markerId)).find().first()
            marker.name = newTitle
        }
    }

    suspend fun deleteItem(markerId: String) {
        withContext(Dispatchers.IO) {
            realm.write {
                val marker = this.query<Marker>("_id == $0", ObjectId(markerId)).find().first()
                delete(marker)
            }
        }
    }

    fun saveImage(markerId: String, uri: Uri, context: Context) {
        val bytes = uriToByteArray(uri, context)
        realm.writeBlocking {
            val marker = this.query<Marker>("_id == $0", ObjectId(markerId)).find().first()
            marker.picture = bytes
        }
    }

    private fun uriToByteArray(uri: Uri, context: Context): ByteArray {
        val inputStream = context.contentResolver.openInputStream(uri)
        val bitmap = BitmapFactory.decodeStream(inputStream)
        val outputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
        return outputStream.toByteArray()
    }

    fun updateMarkerType(markerId: String, newType: String) {
        realm.writeBlocking {
            val marker = this.query<Marker>("_id == $0", ObjectId(markerId)).find().first()
            marker.type = newType
        }
    }
}