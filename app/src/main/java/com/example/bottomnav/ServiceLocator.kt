package com.example.bottomnav


import com.example.mapsprojectreal.daos.MarkersDao
import com.example.bottomnav.realm.RealmManager


object ServiceLocator {
    val realmManager = RealmManager()
    lateinit var markersDao: MarkersDao


    fun configureRealm() {
        requireNotNull(realmManager.realm)
        val realm = realmManager.realm!!
        markersDao = MarkersDao(realm, realmManager.realmApp.currentUser!!.id)
    }
}

