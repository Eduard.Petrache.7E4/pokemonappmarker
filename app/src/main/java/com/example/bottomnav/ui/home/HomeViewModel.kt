package com.example.bottomnav.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.example.bottomnav.ServiceLocator

class HomeViewModel : ViewModel() {
    fun inserToBBDD(name: String, latitude: Double, longitude: Double){
        ServiceLocator.markersDao.insertItem(name, latitude.toString() , longitude.toString())
    }

    fun updateTitleInDB(markerId: String, newTitle: String) {
        ServiceLocator.markersDao.updateTitle(markerId, newTitle)
    }

    val getMarkers = ServiceLocator.markersDao.listFlow().asLiveData()
}