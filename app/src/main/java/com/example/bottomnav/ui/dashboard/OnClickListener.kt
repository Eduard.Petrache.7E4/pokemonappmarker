package com.example.bottomnav.ui.dashboard

import com.example.bottomnav.daos.Marker

interface OnClickListener {
    fun onClick(marker: Marker)
}