package com.example.bottomnav.ui.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.bottomnav.R
import com.example.bottomnav.databinding.FragmentLogBinding

class LogFragment : Fragment() {

    private lateinit var viewModel: LoginViewModel
    private lateinit var binding: FragmentLogBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = FragmentLogBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this)[LoginViewModel::class.java]
        viewModel.loggedIn.observe(viewLifecycleOwner){
            if(it){
                findNavController().navigate(R.id.action_logFragment_to_mapsFragment)
            }
        }

        view.findViewById<Button>(R.id.loginButton).setOnClickListener {

            viewModel.login("Eduard1234"/*binding.inputUsername.text.toString()*/, "Eduard1234"/*binding.inputPassword.text.toString()*/)
        }

        view.findViewById<Button>(R.id.registerButton).setOnClickListener {

            viewModel.register(binding.inputUsername.text.toString(), binding.inputPassword.text.toString())
        }
    }

    companion object {
        fun newInstance() = LogFragment()
    }
}