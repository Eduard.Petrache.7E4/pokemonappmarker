package com.example.bottomnav.ui.dashboard

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView.RecyclerListener
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bottomnav.R
import com.example.bottomnav.databinding.FragmentDashboardBinding
import com.example.bottomnav.ui.detail.DetailFragment

import com.example.bottomnav.daos.Marker

class DashboardFragment : Fragment(), OnClickListener {
    private lateinit var binding: FragmentDashboardBinding
    private lateinit var markerAdapter: MarkerAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var viewModel: DashboardViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this)[DashboardViewModel::class.java]
        viewModel.markerList.observe(viewLifecycleOwner){
            setUpRecyclerView(it)
        }
        val filterOptions = arrayOf("All", "Pokemon", "Station")
        val filterAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, filterOptions)
        filterAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.filterSpinner.adapter = filterAdapter
        binding.filterSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val selectedType = filterOptions[position]
                viewModel.filterMarkers(selectedType)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun setUpRecyclerView(lista: List<Marker>) {
        markerAdapter = MarkerAdapter(lista, this)
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerFragment.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = markerAdapter
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding =  FragmentDashboardBinding.inflate(inflater, container, false)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onClick(marker: Marker) {
        val bundle = Bundle().apply {
            putString("markerId", marker._id.toString())
            putString("latitude", marker.latitude)
            putString("longitude", marker.longitude)
            putString("name", marker.name)
            putByteArray("picture", marker.picture)
        }
        findNavController().navigate(R.id.action_navigation_dashboard_to_detailFragment, bundle)
    }

//    private fun setupFilterSpinner() {
//        val filterOptions = arrayOf("All", "Pokemon", "Station")
//        val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, filterOptions)
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//        binding.filterSpinner.adapter = adapter
//
//        binding.filterSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//                val selectedType = filterOptions[position]
//                viewModel.filterMarkers(selectedType)
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//            }
//        }
//    }

}