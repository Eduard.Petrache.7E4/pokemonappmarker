package com.example.bottomnav.ui.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bottomnav.R
import com.example.bottomnav.databinding.MarkerListBinding
import com.example.bottomnav.daos.Marker

class MarkerAdapter(private val markers: List<Marker>, private val listener: OnClickListener): RecyclerView.Adapter<MarkerAdapter.ViewHolder>() {
    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = MarkerListBinding.bind(view)

        fun setListener(marker: Marker) {
            binding.root.setOnClickListener {
                listener.onClick(marker)
            }
        }
    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.marker_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return markers.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val marker = markers[position]
        with(holder){
            setListener(marker)
            binding.titleMarker.text = marker.name
        }
    }
}