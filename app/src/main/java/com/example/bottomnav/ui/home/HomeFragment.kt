package com.example.bottomnav.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.bottomnav.databinding.FragmentHomeBinding
import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import com.example.bottomnav.R
import com.example.bottomnav.daos.Marker
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener,
    GoogleMap.OnMyLocationClickListener, GoogleMap.OnMapLongClickListener {

    private lateinit var map: GoogleMap
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: HomeViewModel

    private var markers: List<Marker> = emptyList()
    private var markersLiveData: LiveData<List<Marker>>? = null

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.setOnMyLocationButtonClickListener(this)
        map.setOnMyLocationClickListener(this)
        map.setOnMapLongClickListener(this)

        if (!isLocationPermissionGranted()){
            requestLocationPermission()
        } else {
            map.isMyLocationEnabled = true
        }

        markersLiveData = viewModel.getMarkers
        markersLiveData?.observe(viewLifecycleOwner) { newMarkers ->
            markers = newMarkers
            updateMarkersOnMap()
        }
    }

    private fun updateMarkersOnMap() {
        map.clear()
        markers.forEach { marker ->
            val markerOptions = MarkerOptions()
                .title(marker.name)
                .position(LatLng(marker.latitude.toDouble(), marker.longitude.toDouble()))
            map.addMarker(markerOptions)
        }
    }

    override fun onMapLongClick(latLng: LatLng) {
        // Add marker to map
        val markerTitle = "New Marker"
        val markerPosition = latLng
        val markerOptions = MarkerOptions().title(markerTitle).position(markerPosition)
        map.addMarker(markerOptions)

        CoroutineScope(Dispatchers.IO).launch{
            viewModel.inserToBBDD(markerOptions.title!!, markerPosition.latitude, markerPosition.longitude)
        }

        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(markerPosition, 18f),
            5000, null)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        return root
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onMyLocationButtonClick(): Boolean {
        Toast.makeText(requireContext(), "Going to my location", Toast.LENGTH_SHORT).show()
        return false
    }

    override fun onMyLocationClick(p0: Location) {
        Toast.makeText(requireContext(), "You're at ${p0.latitude}, ${p0.longitude}", Toast.LENGTH_SHORT).show()
    }

    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestLocationPermission() {
        requestPermissions(
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            HomeFragment.LOCATION_PERMISSION_REQUEST_CODE
        )
    }
}

