package com.example.bottomnav.ui.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bottomnav.ServiceLocator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginViewModel: ViewModel() {
    val loggedIn = MutableLiveData<Boolean>(false)
    fun register(email: String, password: String){
        CoroutineScope(Dispatchers.IO).launch{
            ServiceLocator.realmManager.register(email, password)
            loggedIn.postValue(true)
        }
    }
    fun login(email: String, password: String) {
        CoroutineScope(Dispatchers.IO).launch{
            ServiceLocator.realmManager.login(email, password)
            loggedIn.postValue(true)
        }
    }
}