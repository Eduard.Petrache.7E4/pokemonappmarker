package com.example.bottomnav.realm


import com.example.bottomnav.daos.Marker
import com.example.bottomnav.ServiceLocator
import io.realm.kotlin.Realm
import io.realm.kotlin.log.LogLevel
import io.realm.kotlin.mongodb.App
import io.realm.kotlin.mongodb.AppConfiguration
import io.realm.kotlin.mongodb.Credentials
import io.realm.kotlin.mongodb.subscriptions
import io.realm.kotlin.mongodb.sync.SyncConfiguration
import io.realm.kotlin.ext.query


class RealmManager {
    val realmApp = App.create(AppConfiguration.Builder("application-1-nlfoz").log(LogLevel.ALL).build())
    var realm : Realm? = null

    fun loggedIn() = realmApp.currentUser?.loggedIn ?: false

    suspend fun register(username: String, password: String) {
        realmApp.emailPasswordAuth.registerUser(username, password)
        login(username, password)
    }

    suspend fun login(username: String, password: String) {
        val creds = Credentials.emailPassword(username, password)
        realmApp.login(creds)
        configureRealm()
    }

    suspend fun configureRealm() {
        requireNotNull(realmApp.currentUser)
        val user = realmApp.currentUser!!
        val config = SyncConfiguration.Builder(user, setOf(Marker::class))
            .initialSubscriptions{ realm ->
                add(
                    realm.query<Marker>(),
                    "All Items"
                )
            }
            .waitForInitialRemoteData()
            .build()
        realm = Realm.open(config)
        realm!!.subscriptions.waitForSynchronization()

        ServiceLocator.configureRealm()

    }

}
