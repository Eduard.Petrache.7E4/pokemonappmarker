package com.example.bottomnav.ui.detail

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.findNavController
import com.example.bottomnav.R
import com.example.bottomnav.ServiceLocator
import com.example.bottomnav.databinding.FragmentDetailBinding
import com.example.bottomnav.ui.dashboard.DashboardViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class DetailFragment : Fragment() {
    private lateinit var binding: FragmentDetailBinding
    private lateinit var viewModel: DashboardViewModel
    private var markerId: String? = null
    private var photoUri: Uri? = null



    companion object {
        private const val REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 1
        private const val REQUEST_IMAGE_PICK = 2
        const val REQUEST_IMAGE_CAPTURE = 1
        const val IMAGE_QUALITY = 80
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = FragmentDetailBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(DashboardViewModel::class.java)

        val markerTypes = arrayOf("All", "Pokemon", "Station")
        val typeAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, markerTypes)
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.typeSpinner.adapter = typeAdapter
        binding.typeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val selectedType = markerTypes[position]
                ServiceLocator.markersDao.updateMarkerType(markerId!!, selectedType)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }


        markerId = arguments?.getString("markerId")
        val markerId = arguments?.getString("markerId")
        val latitude = arguments?.getString("latitude") ?: ""
        val longitude = arguments?.getString("longitude") ?: ""
        val name = arguments?.getString("name") ?: ""
        if (latitude.isNotBlank() && longitude.isNotBlank() && name.isNotBlank()) {
            view.findViewById<TextView>(R.id.titleMarker)?.text = name
            view.findViewById<TextView>(R.id.latitude)?.text = latitude
            view.findViewById<TextView>(R.id.longitude)?.text = longitude
        }

        val pictureByteArray = arguments?.getByteArray("picture")
        if (pictureByteArray != null) {
            val bmp = BitmapFactory.decodeByteArray(pictureByteArray, 0, pictureByteArray.size)
            binding.imageView.setImageBitmap(bmp)
        }

        binding.DeleteButton.setOnClickListener {
            viewModel.deleteMarker(markerId!!)
            findNavController().popBackStack()
        }

        binding.editButton.setOnClickListener {
            enableEditMode()
        }

        binding.saveButton.setOnClickListener {
            val newTitle = binding.titleMarkerEdit.text.toString()
            viewModel.updateMarkerTitle(markerId!!, newTitle)
            binding.titleMarker.visibility = View.VISIBLE
            binding.titleMarker.text = newTitle
            binding.titleMarkerEdit.visibility = View.GONE
            binding.saveButton.visibility = View.GONE
        }

        binding.selectImageButton.setOnClickListener {
            showImageSourceDialog()
        }
//        binding.selectImageButton.setOnClickListener {
//            if (checkPermission()) {
//                openGallery()
//            } else {
//                requestPermission()
//            }
//        }
    }

    private fun checkPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            requireContext(),
            android.Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
            REQUEST_PERMISSION_READ_EXTERNAL_STORAGE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_PERMISSION_READ_EXTERNAL_STORAGE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openGallery()
            } else {
                // Permission denied, show a message or handle it accordingly
            }
        }
    }

    private fun openGallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, REQUEST_IMAGE_PICK)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_PICK && resultCode == Activity.RESULT_OK) {
            data?.data?.let { uri ->
                saveImageToDatabase(markerId!!, uri, requireContext())
            }
        } else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            photoUri?.let { uri ->
                saveImageToDatabase(markerId!!, uri, requireContext())
            }
        }
    }



//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (resultCode == Activity.RESULT_OK) {
//            when (requestCode) {
//                REQUEST_IMAGE_PICK -> {
//                    data?.data?.let { uri ->
//                        saveImageToDatabase(markerId!!, uri)
//                    }
//                }
//                REQUEST_IMAGE_CAPTURE -> {
//                    val bitmap = data?.extras?.get("data") as? Bitmap
//                    bitmap?.let {
//                        saveImageToDatabase(markerId!!, it)
//                    }
//                }
//            }
//        }
//    }



    private fun saveImageToDatabase(markerId: String, uri: Uri, context: Context) {
        viewModel.viewModelScope.launch {
            viewModel.saveImage(markerId, uri, context)
        }
        val bmp = MediaStore.Images.Media.getBitmap(context.contentResolver, uri)
        binding.imageView.setImageBitmap(bmp)
    }


    private fun showImageSourceDialog() {
        val items = arrayOf(getString(R.string.take_new_photo), getString(R.string.select_from_gallery))
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(R.string.select_image_source))
            .setItems(items) { _, which ->
                when (which) {
                    0 -> takePhoto()
                    1 -> selectImageFromGallery()
                }
            }
            .show()
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val storageDir: File? = requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_",
            ".jpg",
            storageDir
        ).apply {
            photoUri = Uri.fromFile(this)
        }
    }


    private fun takePhoto() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(requireContext().packageManager)?.also {
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    null
                }
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        requireContext(),
                        "com.example.bottomnav.fileprovider", // Replace this with your app's actual package name
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                }
            }
        }
    }

//    private fun setupTypeSpinner() {
//        val typeOptions = arrayOf("Pokemon", "Station")
//        val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, typeOptions)
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//        binding.typeSpinner.adapter = adapter
//
//        binding.typeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//                val selectedType = typeOptions[position]
//                viewModel.updateMarkerType(markerId!!, selectedType)
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//            }
//        }
//    }



    private fun selectImageFromGallery() {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, REQUEST_IMAGE_PICK)
    }


    private fun enableEditMode() {
        binding.titleMarker.visibility = View.GONE
        binding.titleMarkerEdit.visibility = View.VISIBLE
        binding.titleMarkerEdit.setText(binding.titleMarker.text)
        binding.saveButton.visibility = View.VISIBLE
    }
}