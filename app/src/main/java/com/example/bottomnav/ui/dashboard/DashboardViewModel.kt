package com.example.bottomnav.ui.dashboard

import android.content.Context
import android.net.Uri
import androidx.lifecycle.*
import com.example.bottomnav.ServiceLocator
import com.example.bottomnav.daos.Marker
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

class DashboardViewModel: ViewModel(){
    private val _markerList = MutableLiveData<List<Marker>>()
    val markerList: LiveData<List<Marker>> get() = _markerList

    init {
        fetchMarkers()
    }

    private fun fetchMarkers() {
        viewModelScope.launch {
            _markerList.value = ServiceLocator.markersDao.listFlow().first()
        }
    }

    fun filterMarkers(type: String) {
        viewModelScope.launch {
            val allMarkers = ServiceLocator.markersDao.listFlow().first()
            _markerList.value = if (type == "All") {
                allMarkers
            } else {
                allMarkers.filter { it.type == type }
            }
        }
    }

//    var markerList = ServiceLocator.markersDao.listFlow().asLiveData()

    fun deleteMarker(markerId: String) {
        viewModelScope.launch {
            ServiceLocator.markersDao.deleteItem(markerId)
        }
    }

    fun updateMarkerTitle(markerId: String, newTitle: String) {
        viewModelScope.launch {
            ServiceLocator.markersDao.updateTitle(markerId, newTitle)
        }
    }

    fun saveImage(markerId: String, uri: Uri, context: Context) {
        viewModelScope.launch {
            ServiceLocator.markersDao.saveImage(markerId, uri, context)
        }
    }
}